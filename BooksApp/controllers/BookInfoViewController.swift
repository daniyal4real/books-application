//
//  BookInfoViewController.swift
//  BooksApp
//
//  Created by Daniyal on 13.03.2021.
//

import UIKit
import Firebase


class BookInfoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    private var bookArray: [BookEntity] = []
    var dataID: String = ""
    private var imageName: String = ""
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: BookInfoCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: BookInfoCell.identifier)
        retriveBookInfo()
    }
    
    
    func retriveBookInfo() {
        let dbReference = Database.database().reference().child("Books").child(dataID)
        dbReference.observe(.value) { (snapshot) in
            let snapshotValue = snapshot.value as? [String: String]
            guard let id = snapshotValue?["id"] else { return }
            guard let bookName = snapshotValue?["book_name"] else { return }
            guard let imageName = snapshotValue?["image_name"] else { return }
            guard let description = snapshotValue?["description"] else { return }
            guard let author = snapshotValue?["author"] else { return }
            self.imageName = imageName
            self.bookArray.append(BookEntity(id: id, bookName: bookName, imageName: self.imageName, description: description, author: author))
            self.tableView.reloadData()
            
    }
  }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: BookInfoCell.identifier, for: indexPath) as! BookInfoCell
        let imageURL  = URL(string: bookArray[indexPath.row].imageName)
        cell.bookImageView.kf.indicatorType = .activity
        cell.bookImageView.kf.setImage(with: imageURL, placeholder: UIImage(contentsOfFile: "image"),
                                       options: [.transition(.flipFromLeft(0.5))])
        cell.bookImageView.contentMode = .scaleAspectFill
        cell.bookImageView.clipsToBounds = true
        cell.bookNameLabel.text = bookArray[indexPath.row].bookName
        cell.authorNameLabel.text = bookArray[indexPath.row].author
        cell.bookDescriptionLabel.text = bookArray[indexPath.row].description
            return cell
    }
    
}
