//
//  NewsViewController.swift
//  BooksApp
//
//  Created by Daniyal on 04.03.2021.
//

import UIKit
import Firebase

class NewsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var books: [BookEntity] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "News"
        tableView.register(UINib(nibName: BookCollectionTableViewCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: BookCollectionTableViewCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        retrieveBooks()
    }
    
    func retrieveBooks() {
        let dbReference = Database.database().reference().child("Books")
        dbReference.observe(.childAdded) { (snapshot) in
            let snapshotValue = snapshot.value as? [String: String]
            guard let id = snapshotValue?["id"] else { return }
            guard let bookName = snapshotValue?["book_name"] else { return }
            guard let imageName = snapshotValue?["image_name"] else { return }
            guard let description = snapshotValue?["description"] else { return }
            guard let author = snapshotValue?["author"] else { return }
            self.books.append(BookEntity(id: id, bookName: bookName, imageName: imageName, description: description, author: author))

            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 350
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BookCollectionTableViewCell.identifier, for: indexPath) as! BookCollectionTableViewCell
        return cell
    }
    
}
