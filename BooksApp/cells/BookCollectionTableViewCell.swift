//
//  BookCollectionTableViewCell.swift
//  BooksApp
//
//  Created by Daniyal on 14.03.2021.
//

import UIKit
import Firebase



class BookCollectionTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    public static let identifier: String = "BookCollectionTableViewCell"
    
    private var books: [BookEntity] = []
    
    
    @IBOutlet var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        collectionView.register(UINib(nibName: BookCollectionViewCell.identifier, bundle: Bundle.main), forCellWithReuseIdentifier: BookCollectionViewCell.identifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        retrieveBooks()
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return books.count
    }
    
    func retrieveBooks() {
        let dbReference = Database.database().reference().child("Books")
        dbReference.observe(.childAdded) { (snapshot) in
            let snapshotValue = snapshot.value as? [String: String]
            guard let id = snapshotValue?["id"] else { return }
            guard let bookName = snapshotValue?["book_name"] else { return }
            guard let imageName = snapshotValue?["image_name"] else { return }
            guard let description = snapshotValue?["description"] else { return }
            guard let author = snapshotValue?["author"] else { return }
            self.books.append(BookEntity(id: id, bookName: bookName, imageName: imageName, description: description, author: author))
//            print(self.books)
            self.collectionView.reloadData()
        }
    }

    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BookCollectionViewCell.identifier, for: indexPath) as! BookCollectionViewCell
        let storageReference = Storage.storage().reference(withPath: "images/\(self.books[indexPath.row].imageName)")
            storageReference.getData(maxSize: 4 * 1024 * 1024) { (data, error) in
                if error != nil {
                    print("Error downloading!")
            }
                if let data = data {
                    cell.bookImage.contentMode = .scaleAspectFill
                    cell.bookImage.clipsToBounds = true
                    cell.bookImage.image = UIImage(data: data)
                }
            }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 210, height: 300)
    }
}
