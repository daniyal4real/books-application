//
//  BookCollectionViewCell.swift
//  BooksApp
//
//  Created by Daniyal on 14.03.2021.
//

import UIKit

class BookCollectionViewCell: UICollectionViewCell {
    
    public static let identifier: String = "BookCollectionViewCell"

    @IBOutlet weak var bookImage: UIImageView!
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        bookImage.layer.cornerRadius = 8
        bookImage.layer.masksToBounds = true 
    }

}
