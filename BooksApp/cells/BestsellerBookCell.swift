//
//  BestsellerBookTableViewCell.swift
//  BooksApp
//
//  Created by Daniyal on 09.03.2021.
//

import UIKit

class BestsellerBookCell: UITableViewCell {
    
    public static let identifier: String = "BestsellerBookCell"
    
    @IBOutlet weak var bookImageView: UIImageView!
    @IBOutlet weak var bookNameLabel: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        bookImageView.layer.cornerRadius = 8
        bookImageView.layer.masksToBounds = true
        
        bookImageView.contentMode = .scaleAspectFit
        bookImageView.clipsToBounds = true
        }
}
