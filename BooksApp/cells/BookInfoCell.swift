//
//  BookInfoImageCell.swift
//  BooksApp
//
//  Created by Daniyal on 13.03.2021.
//

import UIKit

class BookInfoCell: UITableViewCell {
    
    public static let identifier: String = "BookInfoCell"
    
    @IBOutlet weak var bookImageView: UIImageView!
    @IBOutlet weak var bookNameLabel: UILabel!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var bookDescriptionLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        bookImageView.layer.cornerRadius = 8
        bookImageView.layer.masksToBounds = true
        bookImageView.backgroundColor = .blue
        
        bookImageView.contentMode = .scaleAspectFit
        bookImageView.clipsToBounds = true
        
        bookDescriptionLabel.lineBreakMode = .byWordWrapping
        bookDescriptionLabel.numberOfLines = 0
    }
    
}
